const express = require('express');
const app = express();
const path = require('path');
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const port = process.env.PORT || 3000;
const host = process.env.HOST || '0.0.0.0';

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const url = "mongodb+srv://tuta:nIpEpDbrjdKk6CSC@forex.5egj9.mongodb.net/forex?retryWrites=true&w=majority";


const getCurrentRate = () => {
    return Math.floor(Math.random() * 300) + 220;
};

let personDocument = {
    date: new Date().toISOString(),
    rate: getCurrentRate(),
};


function setRateInterval() {
    io.sockets.emit('currency-rate', {
        date: new Date().toISOString(),
        rate: getCurrentRate(),
    });
}
setInterval(() => {
    personDocument = {
        date: new Date().toISOString(),
        rate: getCurrentRate(),
    }
    io.sockets.emit('currency-rate', personDocument);
    io.sockets.emit('buyHistory', history);
}, 5000);
let history =[];
function getHistory() {
    {
        MongoClient.connect(url, function(err, client) {
            const db = client.db("forex");
            db.collection('buyHistory').find({}).toArray(function(err, result) {
                if (err) throw err;
                console.log(result);
                history = result;
            });
            assert.equal(null, err);
            client.close();
        });
    }
}
function setBuyHistory(payload) {
    MongoClient.connect(url, function(err, client) {
        const db = client.db("forex");
        db.collection('buyHistory').insertOne(payload)
            .then(function(result) {
                console.log(result);
                getHistory()
                io.sockets.emit('buyHistory', history);
            });
        assert.equal(null, err);
        client.close();
    });
}


io.on('connection', (socket) => {
    getHistory();

    socket.balance = {usd: 10000, facebook: 10};

    socket.emit('balance-changed', socket.balance);

    socket.on('buy', (payload) => {
        if (personDocument.rate === payload.rate) {
            const sum = payload.rate * payload.amount;
            const newBalanceUsd = socket.balance.usd - sum;

            if (newBalanceUsd > 0) {
                socket.balance = {
                    usd : newBalanceUsd,
                    facebook: socket.balance.facebook + Number(payload.amount)
                };

                socket.emit('balance-changed', socket.balance);
            }
            payload.sum = sum;
            payload.type = 'buy';
            setBuyHistory(payload);
        }
    });

    socket.on('sell', (payload) => {
        const newBalanceFacebook = socket.balance.facebook - payload.amount;

        if (newBalanceFacebook >= 0) {
            socket.balance = {
                usd: socket.balance.usd +  (payload.rate * payload.amount),
                facebook: newBalanceFacebook
            };

            socket.emit('balance-changed', socket.balance);
            payload.sum = payload.rate * payload.amount;
            payload.type = 'sell';
            setBuyHistory(payload);
        }
    });
});

server.listen(port, host, () => {
    console.log('Server listening at %s:%d', host, port);
});

app.use(express.static(path.join(__dirname, '../client')));

